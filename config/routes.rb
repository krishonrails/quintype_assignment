Rails.application.routes.draw do
  root to: 'cabs#index'
  constraints format: :json do
    resources :trips
    resources :cabs
  end
  match '*path', to: 'application#page_not_found', via: :all
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end