# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

10.times do |index|
  is_pink = index.even? ? true : false
  Cab.create(reg_number: "REGNO-#{(index+1121)*(index+1)}", is_assigned: false, latitude: "5#{index+1}" + '.' + "#{index+1}", longitude: "5#{index*1}" + '.' + "#{index+1}", is_pink: is_pink)
end