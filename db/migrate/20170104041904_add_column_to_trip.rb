class AddColumnToTrip < ActiveRecord::Migration[5.0]
  def change
    add_column :trips, :cost, :float
    add_column :trips, :trip_time, :string
  end
end
