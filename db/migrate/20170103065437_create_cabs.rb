class CreateCabs < ActiveRecord::Migration[5.0]
  def change
    create_table :cabs do |t|
      t.string :reg_number
      t.boolean :is_assigned, default: false
      t.float :latitude
      t.float :longitude
      t.boolean :is_pink, default: false

      t.timestamps
    end
  end
end
