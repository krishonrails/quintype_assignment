class CreateTrips < ActiveRecord::Migration[5.0]
  def change
    create_table :trips do |t|
      t.float :start_latitude
      t.float :end_latitude
      t.float :start_longitude
      t.float :stop_longitude
      t.datetime :start_time
      t.datetime :end_time
      t.float :trip_distance
      t.references :cab, foreign_key: true

      t.timestamps
    end
  end
end
