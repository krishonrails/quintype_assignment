class Trip < ApplicationRecord
  PERKM = 1
  PERMINUTE = 2
  PINK = 5
  attr_accessor :is_pink
  belongs_to :cab
  after_create :assign_cab
  before_validation :get_cab, if: :new_record?

  def get_cab
    cab = Cab.find_nearest_cab(self.start_latitude.to_f, self.start_longitude.to_f, self.is_pink)
    if cab.present?
      self.start_time = Time.zone.now
      self.cab = cab
    else
      errors.add(:cab, :blank, message: "Cabs are not available")
    end
  end

  def assign_cab
    self.cab.assign
  end

  def travel_cost(end_latitude, stop_longitude)
    cab = self.cab
    cab.trip_end(end_latitude, stop_longitude)
    trip_cost
  end

  def trip_cost
    time = trip_time
    distance = cab.find_distance(end_latitude, stop_longitude, self.start_latitude, self.start_longitude)
    cost = time*1 +  distance*2
    cost = cost + 5 if self.cab.is_pink
    return distance, time, cost
  end

  def trip_time
    (end_time - start_time)/60
  end
end