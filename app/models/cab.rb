class Cab < ApplicationRecord

  def find_distance(a1, b1, a2, b2)
    ((a2 - a1)**2 + (b2 - b1)**2)**0.5
  end

  def assign
    update(is_assigned: true)
  end

  def trip_end(end_latitude, stop_longitude)
    #Should be available at the drop location once the trip ends.
    update(is_assigned: false, latitude: end_latitude, longitude: stop_longitude)
  end

  class << self
    def find_nearest_cab(start_latitude, start_longitude, is_pink)
      conditions = {}
      conditions[:is_pink] = true if is_pink == true
      cabs = Cab.is_not_assigned(conditions)
      Cab.distance_between?(cabs, start_latitude, start_longitude) if cabs.present?
    end

    def distance_between?(cabs, start_latitude, start_longitude)
      cabs.inject do |cab1, cab2|
        cab1.find_distance(start_latitude, start_longitude, cab1.latitude, cab1.longitude) < cab2.find_distance(start_latitude, start_longitude, cab2.latitude, cab2.longitude) ? cab1 : cab2
      end
    end

    def is_not_assigned(conditions = {})
      conditions[:is_assigned] = false
      where(conditions)
    end
  end
end