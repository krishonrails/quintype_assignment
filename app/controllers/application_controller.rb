class ApplicationController < ActionController::Base
  # protect_from_forgery with: :exception
  rescue_from ::ActiveRecord::RecordNotFound, with: :record_not_found


  def page_not_found
    redirect_to "/"
  end

  protected

  def record_not_found(e)
    render json: { error: e.message }, status: :not_found
  end
end