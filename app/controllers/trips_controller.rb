class TripsController < ApplicationController
  before_action :set_trip, only: [:show, :edit, :update, :destroy]

  # GET /trips
  # GET /trips.json
  def index
    @trips = Trip.all
  end

  # GET /trips/1
  # GET /trips/1.json
  def show
  end

  # GET /trips/new
  def new
    @trip = Trip.new
  end

  # GET /trips/1/edit
  def edit
  end

  # POST /trips
  # POST /trips.json
  def create
    errors = []
    errors << 'start_latitude reuiqred' if params['trip']['start_latitude'].blank?
    errors << 'start_longitude reuiqred' if params['trip']['start_longitude'].blank?
    if errors.blank?
      @trip = Trip.new(trip_params)
      if @trip.save
        render :show, status: :created
      else
        render json: { message: @trip.errors }, status: :unprocessable_entity
      end
    else
      render json: { message: errors }, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /trips/1
  # PATCH/PUT /trips/1.json
  def update
    errors = []
    end_latitude = params['trip']['end_latitude'].to_f
    stop_longitude = params['trip']['stop_longitude'].to_f
    errors << 'end_latitude reuiqred' if end_latitude.blank?
    errors << 'stop_longitude reuiqred' if stop_longitude.blank?
    distance, time, cost =  @trip.travel_cost(end_latitude, stop_longitude)
    if errors.blank?
      if @trip.update(end_latitude: end_latitude, stop_longitude: stop_longitude, end_time: Time.zone.now, trip_distance: distance, cost: cost, trip_time: time)
        render :show, status: :ok
      else
        render json: @trip.errors, status: :unprocessable_entity
      end
    else
      render json: @trip.errors, status: :unprocessable_entity
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_trip
    @trip = Trip.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def trip_params
    params.require(:trip).permit(:start_latitude, :end_latitude, :start_longitude, :stop_longitude, :is_pink)
  end
end