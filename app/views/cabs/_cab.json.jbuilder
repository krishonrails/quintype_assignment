json.extract! cab, :id, :reg_number, :is_assigned, :latitude, :longitude, :is_pink, :created_at, :updated_at
json.url cab_url(cab, format: :json)